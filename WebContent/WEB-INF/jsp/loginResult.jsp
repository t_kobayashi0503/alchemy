<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.User" %>
<%
//セッションスコープからユーザー情報を取得
User loginUser = (User) session.getAttribute("loginUser");
%>
<jsp:include page="/header.jsp" />
<body>
<% if(loginUser != null) { %>
<div class="common_layout login_result">
	<h1>アイテム錬金ジェネレータ</h1>
	<p class="sub_menu"><a href="/alchemy/Logout">ログアウト</a></p>
	<div class="center"><br /><br />
		<p>ようこそ<%= loginUser.getName() %>さん。<br />
		ここでは道具や材料を使って様々なアイテムを作れます。<br /><br /><br /><br /></p>
		<form action="/alchemy/Main" method="post">
			<p class="material_select">選択肢①</p>
			<ul class="material_list">
				<li><input type="radio" name="item_select1" value="藁">藁</li><!-- straw -->
				<li><input type="radio" name="item_select1" value="紙">紙</li><!-- paper -->
				<li><input type="radio" name="item_select1" value="風船">風船</li><!-- balloon -->
				<li><input type="radio" name="item_select1" value="棒">棒</li><!-- pole -->
				<li><input type="radio" name="item_select1" value="米">米</li><!-- rice -->
				<li><input type="radio" name="item_select1" value="バター">バター</li><!-- butter -->
				<li><input type="radio" name="item_select1" value="イチゴ">イチゴ</li><!-- strawberry -->
				<li><input type="radio" name="item_select1" value="木">木</li><!-- tree -->
			</ul>
			<p class="material_select">選択肢②</p>
			<ul class="material_list">
				<li><input type="radio" name="item_select2" value="竹">竹</li><!-- banboo -->
				<li><input type="radio" name="item_select2" value="紐">紐</li><!-- rope -->
				<li><input type="radio" name="item_select2" value="箱">箱</li><!-- box -->
				<li><input type="radio" name="item_select2" value="月">月</li><!-- moon -->
				<li><input type="radio" name="item_select2" value="星">星</li><!-- star -->
				<li><input type="radio" name="item_select2" value="火">火</li><!-- fire -->
				<li><input type="radio" name="item_select2" value="砂糖">砂糖</li><!-- sugar -->
				<li><input type="radio" name="item_select2" value="ミカン">ミカン</li><!-- orange -->
				<li><input type="radio" name="item_select2" value="卵">卵</li><!-- egg -->
			</ul>
			<p class="material_select">選択肢③</p>
			<ul class="material_list">
				<li><input type="radio" name="item_select3" value="花">花</li><!-- flour -->
				<li><input type="radio" name="item_select3" value="葉っぱ">葉っぱ</li><!-- leaf -->
				<li><input type="radio" name="item_select3" value="ペン">ペン</li><!-- pen -->
				<li><input type="radio" name="item_select3" value="糸">糸</li><!-- yarn -->
				<li><input type="radio" name="item_select3" value="着色料">着色料</li><!-- coloring -->
				<li><input type="radio" name="item_select3" value="飾り">飾り</li><!-- ornament -->
				<li><input type="radio" name="item_select3" value="水">水</li><!-- water -->
				<li><input type="radio" name="item_select3" value="小麦">小麦</li><!-- wheat -->
				<li><input type="radio" name="item_select3" value="海苔">海苔</li><!-- seaweed -->
				<li><input type="radio" name="item_select3" value="牛乳">牛乳</li><!-- milk -->
				<li><input type="radio" name="item_select3" value="火薬">火薬</li><!-- gunpowder -->
				<li><input type="radio" name="item_select3" value="ガラス">ガラス</li><!-- glass -->
			</ul>
			<p><button type="submit" class="btn-square-slant">アイテムを錬成する</button></p>
		</form>
	</div>
</div>
<% } else { %>
<div class="common_layout logout">
	<h1>アイテム錬金ジェネレータ</h1>
	<p class="center">ログインに失敗しました。</p><br /><br /><br />
	<p class="center"><a href="/alchemy/">トップへ</a></p>
</div>
<% } %>
<jsp:include page="/footer.jsp" />
</body>
</html>