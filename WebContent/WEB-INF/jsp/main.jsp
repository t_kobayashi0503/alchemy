<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>どこつぶ</title>
<link rel="stylesheet" type="text/css" href="css/docoTsubu.css">
</head>
<body>
<div class="common_layout login_result">
	<h1>どこつぶメイン</h1>
	<p><c:out value="${loginUser.name}" />さん、ログイン中</p>
	<p class="sub_menu"><a href="/docoTsubu/Logout">ログアウト</a>
	<a href="/docoTsubu/Main">更新</a></p>
	<form action="/docoTsubu/Main" method="post">
	<input type="text" name="text">
	<input type="submit" value="つぶやく">
	</form>

	<ul>
		<c:if test="${not empty errorMsg}">
			<li>${errorMsg}</li>
		</c:if>
		<c:forEach var="mutter" items="${mutterList}">
			<li><c:out value="${mutter.userName}" />:<c:out value="${mutter.text}" /></li>
		</c:forEach>
	</ul>
</div>
</body>
</html>