<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="/header.jsp" />
<body>
<div class="common_layout logout">
	<h1>アイテム錬金ジェネレータ</h1>
	<p class="center">ログアウトしました。<br /><br /><br /></p>
	<p class="center"><a href="/alchemy/">トップへ</a></p>
</div>
<jsp:include page="/footer.jsp" />
</body>
</html>