<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/header.jsp" />
<body>
<div class="common_layout login_result">
	<h1>アイテム錬金ジェネレータ</h1>
	<p class="sub_menu"><a href="/alchemy/Logout">ログアウト</a></p>
	<div class="item_detail">
		<c:choose>
			<c:when test="${resultItem != null}">
				<h2 class="item_name">${resultItem.itemName}</h2>
				<p class="center"><img class="item_img" src="img/${resultItem.itemImg}.png" alt="${resultItem.itemName}" /></p>
				<p class="item_desc">${resultItem.itemDesc}</p>
			</c:when>
			<c:otherwise>
				<h2 class="item_name">闇鍋</h2>
				<p class="center"><img class="item_img" src="img/food_yaminabe.png" alt="闇鍋" /></p>
				<p class="item_desc">何が入っているか不明な鍋。</p>
			</c:otherwise>
		</c:choose>
	</div>
	<form action="/alchemy/Main" method="get">
	<p class="center" style="margin-top:30px;"><button type="submit">もう一度錬成する</button></p>
	</form>
</div>
<jsp:include page="/footer.jsp" />
</body>
</html>