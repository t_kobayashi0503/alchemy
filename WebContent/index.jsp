<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="/header.jsp" />
<body>
<div class="common_layout login">
	<h1>アイテム錬金ジェネレータ</h1>
	<form action="/alchemy/Login" method="post">
		<div class="cp_iptxt">
			<input type="text" name="name" placeholder="ユーザー名">
			<i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i>
		</div>
		<div class="cp_iptxt">
			<input type="password" name="pass" placeholder="パスワード">
			<i class="fa fa-lock fa-lg fa-fw" aria-hidden="true"></i>
		</div>
		<button type="submit" name="submit" value="送信" class="submit">ログイン</button>
	</form>
</div>
<jsp:include page="/footer.jsp" />
</body>
</html>