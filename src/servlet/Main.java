package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import model.SelectItem;
import model.SelectItemLogic;
import model.User;

/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		//ログインしているか確認するためセッションスコープからユーザー情報を取得
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if(loginUser == null) { //ログインしていない場合
			//リダイレクト
			response.sendRedirect("/alchemy/");
		} else { //ログイン済みの場合
			//フォワード
			RequestDispatcher dispatcher =
					request.getRequestDispatcher("/WEB-INF/jsp/loginResult.jsp");
			dispatcher.forward(request, response);
		}
	}
	protected void doPost(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		//リクエストパラメータの取得
		request.setCharacterEncoding("UTF-8");

		String item1 = request.getParameter("item_select1");
		String item2 = request.getParameter("item_select2");
		String item3 = request.getParameter("item_select3");

		//確認用の出力
		System.out.println(item1 + item2 + item3);

		SelectItem selectItem = new SelectItem(item1, item2, item3);

		//アイテムを取得して、リクエストスコープに保存
		SelectItemLogic selectItemLogic = new SelectItemLogic();
		Item resultItem = selectItemLogic.execute(selectItem);
		if(resultItem != null) { //ログインしていない場合
			request.setAttribute("resultItem",resultItem);
		} else { //ログイン済みの場合
		}
		//メイン画面にフォワード
		RequestDispatcher dispatcher =
				request.getRequestDispatcher("/WEB-INF/jsp/itemGeneration.jsp");
		dispatcher.forward(request, response);
	}
}
