package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Item;
import model.SelectItem;

public class ItemDAO {

	final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
	final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
	final String DB_NAME = "alchemy";//データベース名
	final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防止
	final String JDBC_URL =DB_URL+DB_NAME+DB_ENCODE;//接続DBとURL
	final String DB_USER = "root";//ユーザーID
	final String DB_PASS = "root";//パスワード

	//データベースのアイテムを全て取得
	public List<Item> findAll() {
		List<Item> itemList = new ArrayList<>();

		//データベース接続
		try {
			Class.forName(DRIVER_NAME);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)) {
			//SELECT文の準備
			String sql = "select item_material1,item_material2,item_material3 from item order by id desc";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			//SELECTを実行
			ResultSet rs = pStmt.executeQuery();

			//SELECT文の結果をArrayListに格納
			while (rs.next()) {
				String item_id = rs.getString("item_id");
				String item_name = rs.getString("item_name");
				String item_img = rs.getString("item_img");
				String item_material1 = rs.getString("item_material1");
				String item_material2 = rs.getString("item_material2");
				String item_material3 = rs.getString("item_material3");
				String item_desc = rs.getString("item_desc");
				Item item = new Item(item_id, item_name, item_img, item_material1, item_material2, item_material3, item_desc);
				itemList.add(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return itemList;
	}

	//データベースから特定のアイテムを取得
	public Item findByItem(SelectItem selectItem) {
		Item item = null;
		//データベース接続
		try {
			Class.forName(DRIVER_NAME);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)) {
			String sql = "select item_name,item_img,item_desc from item where item_material1=? and item_material2=? and item_material3=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, selectItem.getSelectItem1());
			pStmt.setString(2, selectItem.getSelectItem2());
			pStmt.setString(3, selectItem.getSelectItem3());

			//確認用の出力
			System.out.println(selectItem.getSelectItem1() + selectItem.getSelectItem2() + selectItem.getSelectItem3());

			ResultSet rs = pStmt.executeQuery();

			if(rs.next()) {
				String itemName = rs.getString("item_name");
				String itemImg = rs.getString("item_img");
				String itemDesc = rs.getString("item_desc");
				item = new Item(itemName, itemImg, itemDesc);

				//確認用の出力
				System.out.println(itemName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return item;
	}
}
