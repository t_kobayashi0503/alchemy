package model;

import java.io.Serializable;

public class SelectItem implements Serializable {
	private String selectItem1; //選択肢①
	private String selectItem2; //選択肢②
	private String selectItem3; //選択肢③
	//コンストラクタ
	public SelectItem(String selectItem1, String selectItem2, String selectItem3) {
		this.selectItem1 = selectItem1;
		this.selectItem2 = selectItem2;
		this.selectItem3 = selectItem3;
	}
	//getter
	public String getSelectItem1() {return selectItem1;}
	public void setSelectItem1(String selectItem1) {this.selectItem1 = selectItem1;}
	public String getSelectItem2() {return selectItem2;}
	public void setSelectItem2(String selectItem2) {this.selectItem2 = selectItem2;}
	public String getSelectItem3() {return selectItem3;}
	public void setSelectItem3(String selectItem3) {this.selectItem3 = selectItem3;}
}