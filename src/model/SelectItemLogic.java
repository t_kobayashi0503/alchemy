package model;

import dao.ItemDAO;

public class SelectItemLogic {

	public Item execute(SelectItem selectItem){
		ItemDAO dao = new ItemDAO();
		Item item = dao.findByItem(selectItem);
		return item;
	}
}
