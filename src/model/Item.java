package model;

import java.io.Serializable;

public class Item implements Serializable {
	private String itemId; //アイテムID
	private String itemName; //アイテム名
	private String itemImg; //アイテム画像
	private String itemMaterial1; //アイテム材料1
	private String itemMaterial2; //アイテム材料2
	private String itemMaterial3; //アイテム材料3
	private String itemDesc; //アイテム説明
	//コンストラクタ
	public Item(){}
	public Item(String itemName, String itemImg, String itemDesc) {
		this.itemName = itemName;
		this.itemImg = itemImg;
		this.itemDesc = itemDesc;
	}
	public Item(String itemId, String itemName, String itemImg, String itemMaterial1, String itemMaterial2,
			String itemMaterial3, String itemDesc) {
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemImg = itemImg;
		this.itemMaterial1 = itemMaterial1;
		this.itemMaterial2 = itemMaterial2;
		this.itemMaterial3 = itemMaterial3;
		this.itemDesc = itemDesc;
	}
	//getter,setter
	public String getItemId() {return itemId;}
	public void setItemId(String itemId) {this.itemId = itemId;}
	public String getItemName() {return itemName;}
	public void setItemName(String itemName) {this.itemName = itemName;}
	public String getItemImg() {return itemImg;}
	public void setItemImg(String itemImg) {this.itemImg = itemImg;}
	public String getItemMaterial1() {return itemMaterial1;}
	public void setItemMaterial1(String itemMaterial1) {this.itemMaterial1 = itemMaterial1;}
	public String getItemMaterial2() {return itemMaterial2;}
	public void setItemMaterial2(String itemMaterial2) {this.itemMaterial2 = itemMaterial2;}
	public String getItemMaterial3() {return itemMaterial3;}
	public void setItemMaterial3(String itemMaterial3) {this.itemMaterial3 = itemMaterial3;}
	public String getItemDesc() {return itemDesc;}
	public void setItemDesc(String itemDesc) {this.itemDesc = itemDesc;}
}
